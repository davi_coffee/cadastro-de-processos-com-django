from django.contrib import admin
from django.urls import path, include

app_name = 'core'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('processos.core.urls')),
    path('processos/', include('processos.descricao.urls')),
    path('comentario/', include('processos.comment.urls')),
     path('calendario/', include('processos.cal.urls')),
]
