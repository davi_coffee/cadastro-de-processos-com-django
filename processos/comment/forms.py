from django import forms
from .models import Comments



"""
class NameForm(forms.Form):
    your_name = forms.CharField(label='Your name', max_length=100)

"""
class CommentsForm(forms.ModelForm):

    class Meta:
        model = Comments
        fields = '__all__' # todos os campos
