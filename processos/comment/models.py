from django.db import models
from django.urls import reverse_lazy
from processos.descricao.models import Description
from processos.core.models import TimeStampedModel


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_{0}/{1}'.format(instance.user.id, filename)


class Comments(TimeStampedModel):
    n_processo = models.ForeignKey(Description, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=200, blank=True, null=True)
    comment = models.TextField('Observações', blank=True, null=True)

    class Meta:
        """Ordering é uma tupla que contém os índices dos campos. """
        ordering = ('-created',)

    def __str__(self):
        # Returna id_comments - id_description - cliente - natureza
        return '{} : Proc-{}'.format(self.n_processo.id, self.n_processo.pasta)

    def get_absolute_url(self):
        return reverse_lazy('comment:comment_detail', kwargs={'pk': self.pk})

    def cliente_formatado(self):
        nome = self.n_processo.cliente

        return str(nome.title())




