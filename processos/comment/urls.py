from django.urls import path
from . import views

app_name = 'comment'


urlpatterns = [
    path('<int:pk>/', views.processo_comment_detail, name='processo_comment_detail'),
    path('<int:pk>/deletar', views.comment_delete, name='comment_delete'),
    path('<int:pk>/editar', views.comment_update, name='comment_update'),
    path('<int:pk>/event', views.comment_event, name='comment_event'),
    path('<int:pk>/add/', views.processo_comment_form, name='processo_comment_form'),
]