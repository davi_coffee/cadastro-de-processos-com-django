from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from .models import Comments
from processos.descricao.models import Description
from django.contrib.auth.decorators import login_required

from .forms import CommentsForm
from django.views.generic import CreateView, UpdateView

from processos.cal.models import Event


@login_required(login_url='core:logar_user')
def processo_comment_detail(request, pk):
    template_name = 'processo_comment_detail.html'
    descrip = Description.objects.get(id=pk)
    obj = Comments.objects.filter(n_processo=pk)
    # event = Event.objects.filter(id_comment=pk)

    # context = {'obj_list_detail': obj, 'cliente_detail': descrip, 'event': event}
    context = {'obj_list_detail': obj, 'cliente_detail': descrip}

    return render(request, template_name, context)


def comment_delete(request, pk):
    comentario = Comments.objects.get(pk=pk)
    id = comentario.n_processo.id
    comentario.delete()
    return redirect('comment:processo_comment_detail', pk=id)


def comment_update(request, pk):
    template_name = 'processo_comment_edit.html'
    comentario = get_object_or_404(Comments, pk=pk)
    id = comentario.n_processo.id
    description = Description.objects.get(id=id)
    form = CommentsForm(instance=comentario)

    if request.method == 'POST':
        form = CommentsForm(request.POST, instance=comentario)

        if form.is_valid():
            comentario = form.save(commit=False)
            comentario.n_processo = form.cleaned_data['n_processo']
            comentario.titulo = form.cleaned_data['titulo']
            comentario.comment = form.cleaned_data['comment']

            comentario.save()
            return redirect('comment:processo_comment_detail', pk=id)
        else:
            return render(request, template_name, {'comentario': comentario, 'form': form})

    elif request.method == 'GET':
        return render(request, template_name, {'comentario': comentario, 'form': form})


def comment_event(request, pk):
    template_name = 'comment_event.html'
    obj = Event.objects.filter(id_comment=pk)

    context = {'comment_event': obj, 'id': pk}  # Dict que possui as descrições dos objetos da classe produto

    return render(request, template_name, context)


def processo_comment_form(request, pk):
    template_name = 'processo_comment_form.html'
    description = Description.objects.get(id=pk)

    if request.method == "POST":
        form = CommentsForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('comment:processo_comment_detail', pk=pk)

    else:
        form = CommentsForm()

    context = {'cliente_detail': description, 'form': form}

    return render(request, template_name, context)

