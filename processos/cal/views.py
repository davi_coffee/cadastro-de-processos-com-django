from datetime import timedelta, date
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from django.utils.safestring import mark_safe
import calendar

from processos.cal.models import *
from processos.cal.utils import Calendar
from processos.cal.forms import EventForm
from processos.descricao.models import Description


def index(request):
    return HttpResponse('hello')


class CalendarView(generic.ListView):
    model = Event
    template_name = 'cal/calendar.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        d = get_date(self.request.GET.get('month', None))
        cal = Calendar(d.year, d.month)
        html_cal = cal.formatmonth(withyear=True)
        context['calendar'] = mark_safe(html_cal)
        context['prev_month'] = prev_month(d)
        context['next_month'] = next_month(d)
        context['id_event'] = Event.objects.all()
        return context


def get_date(req_month):
    if req_month:
        year, month = (int(x) for x in req_month.split('-'))
        return date(year, month, day=1)
    return date.today()


def prev_month(d):
    first = d.replace(day=1)
    prev_month = first - timedelta(days=1)
    month = 'month=' + str(prev_month.year) + '-' + str(prev_month.month)
    return month


def next_month(d):
    days_in_month = calendar.monthrange(d.year, d.month)[1]
    last = d.replace(day=days_in_month)
    next_month = last + timedelta(days=1)
    month = 'month=' + str(next_month.year) + '-' + str(next_month.month)
    return month


def event(request, event_id=None, id_cliente=None):
    instance = Event()
    # Vai para link
    if event_id:
        # Link com id = event_id
        instance = get_object_or_404(Event, pk=event_id)
    else:
        instance = Event()

    form = EventForm(request.POST or None, instance=instance)

    if request.POST and form.is_valid():
        form.save()
        mensagem = Event.objects.last()
        return HttpResponseRedirect(reverse('cal:calendar'))

    id = Description.objects.get(id=id_cliente)

    context = {'form': form, 'id_cliente': id}

    return render(request, 'cal/event.html', context)


def list_event(request):
    template_name = 'cal/list_event.html'
    obj = Description.objects.all()

    """
    now = datetime.now()
    day_now = now.day
    month = now.strftime("%b")
    list_date = [day_now, month]
    """

    list = Event.objects.all()

    context = {'lista_event': list, 'description': obj}

    return render(request, template_name, context)
