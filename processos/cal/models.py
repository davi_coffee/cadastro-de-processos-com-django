from django.db import models
from django.urls import reverse, reverse_lazy
from processos.descricao.models import Description
import datetime


class Event(models.Model):
    id_comment = models.ForeignKey(Description, on_delete=models.CASCADE, verbose_name='Cliente')
    title = models.CharField('Título', max_length=200)
    description = models.TextField('Descrição')
    start_time = models.DateTimeField('Horário Inicial')
    end_time = models.DateTimeField('Horário Final')

    @property
    def get_html_url(self):
        # url link na pagina calendar/
        href = '#ID' + str(self.id)
        return f'<a data-toggle="modal" href="{href}" id="link"> {self.title} </a>'

    def format_date(self):
        diferenca = datetime.timedelta(hours=-3)
        fuso_horario = datetime.timezone(diferenca)
        tempo = self.start_time
        tempo_fuso = tempo.astimezone(fuso_horario)

        tempo_start = tempo_fuso.strftime("%d de %b  %H:%M")
        return tempo_start

    def format_date_end(self):
        diferenca = datetime.timedelta(hours=-3)
        fuso_horario = datetime.timezone(diferenca)
        tempo = self.end_time
        tempo_fuso = tempo.astimezone(fuso_horario)

        tempo_start = tempo_fuso.strftime("%d de %b  %H:%M")
        return tempo_start

    def prazo_encerrado(self):
        data_now = datetime.datetime.now()
        day_event = self.start_time.day
        month_event = self.start_time.month

        if 0 <= self.start_time.hour <= 2:
            day_event = day_event - 1

        if (day_event >= data_now.day) and (month_event >= data_now.month):
            return True
        else:
            return False


    def prazo_hoje(self):
        data_now = datetime.datetime.now()
        day_now = self.start_time.day

        if 0 <= self.start_time.hour <= 2:
            day_now = day_now - 1

        month_event = self.start_time.month

        if (day_now == data_now.day) and (month_event == data_now.month):
            return True
        else:
            return False

    def prazo_amanha(self):
        data_now = datetime.datetime.now()
        day_now = self.start_time.day

        if 0 <= self.start_time.hour <= 2:
            day_now = day_now - 1

        month_event = self.start_time.month
        amanha = int(data_now.day + 1)

        if (day_now == amanha) and (month_event == data_now.month):
            return True
        else:
            return False

