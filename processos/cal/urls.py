from django.conf.urls import url
from . import views

app_name = 'cal'

urlpatterns = [
    url(r'^index/$', views.index, name='index'),
    url(r'^calendar/$', views.CalendarView.as_view(), name='calendar'),
    url(r'^event/$', views.list_event, name='list_event'),
    url(r'^event/new/$', views.event, name='event_new'),
    url(r'^event/new/(?P<id_cliente>\d+)/$', views.event, name='event_new'),
]