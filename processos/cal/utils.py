from datetime import datetime, timedelta, date
from .calendar_pt import HTMLCalendar
from .models import Event

class Calendar(HTMLCalendar):
	def __init__(self, year=None, month=None):
		self.year = year
		self.month = month
		super(Calendar, self).__init__()

	# formats a day as a td
	# filter events by day
	def formatday(self, day, events):
		events_per_day = events.filter(start_time__day=day)
		d = ''
		for event in events_per_day:
			# link das evento criado
			d += f'<div class="card"><li style="list-style-type:none; text-transform: capitalize; text-align: center;">' \
				 f'{event.get_html_url}' \
				 f'</li></div>'

		if day != 0:
			day_now = datetime.now().day
			month_day = datetime.now().month

			if day == day_now and self.month == month_day:
				return f"<td class='back-table-now'><span class='date-now'>{day}&nbsp</span><ul style='padding-top: 10px;><span style='font-size: 20px;'>{d}</span></ul></td>"
			else:
				return f"<td class='back-table'><span class='date'>{day}&nbsp</span><ul style='padding-top: 10px;'>{d}</ul></td>"
		return '<td class="none-week"></td>'

	# formats a week as a tr
	def formatweek(self, theweek, events):
		week = ''
		for d, weekday in theweek:
			week += self.formatday(d, events)
		return f'<tr class="semana"> {week} </tr>'

	# formats a month as a table
	# filter events by year and month
	def formatmonth(self, withyear=True):
		events = Event.objects.filter(start_time__year=self.year, start_time__month=self.month)

		cal = f'<table border="0" cellpadding="0" cellspacing="0" class="calendar">\n'
		cal += f'{self.formatmonthname(self.year, self.month, withyear=withyear)}\n'
		cal += f'{self.formatweekheader()}\n'
		for week in self.monthdays2calendar(self.year, self.month):
			cal += f'{self.formatweek(week, events)}\n'
		return cal