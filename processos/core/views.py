from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import  AuthenticationForm, UserCreationForm, PasswordChangeForm


@login_required(login_url='core:logar_user')
def index(request):
    template_name = 'index.html'
    return render(request,template_name)


def logar_user(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        usuario = authenticate(request, username=username, password=password)
        if usuario is not None:
            login(request, usuario)
            return redirect('core:index')
        else:
            form_login = AuthenticationForm()
    else:
        form_login = AuthenticationForm()
    return render(request, 'login.html', {'form_login': form_login})


def cadastrar_user(request):
    if request.method == 'POST':
        form_user = UserCreationForm(request.POST)
        if form_user.is_valid():
            form_user.save()
            return redirect('core:index')

    else:
        form_user = UserCreationForm()

    context = {'form_user': form_user}
    return render(request, 'cadastro.html', context)


@login_required(login_url='core:logar_user')
def deslogar_user(request):
    logout(request)
    return redirect('core:logar_user')


@login_required(login_url='core:logar_user')
def alterar_senha(request):
    if request.method == "POST":
        form_senha = PasswordChangeForm(request.user, request.POST)
        if form_senha.is_valid():
            user = form_senha.save()
            update_session_auth_hash(request, user)
            return redirect('core:index')
    else:
        form_senha = PasswordChangeForm(request.user)
    return render(request, 'alterar_senha.html', {'form_senha': form_senha})

