from django.urls import path
from . import views
from django.contrib import admin

app_name = 'core'

urlpatterns = [
    path('home/', views.index, name='index'),
    path('cadastrar_usuario/',views.cadastrar_user, name="cadastrar_user"),
    path('deslogar_usuario/', views.deslogar_user, name="deslogar_user"),
    path('alterar_senha/', views.alterar_senha, name="alterar_senha"),
    path('', views.logar_user, name="logar_user"),
]