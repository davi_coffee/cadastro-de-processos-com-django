from django.db import models

class TimeStampedModel(models.Model):
    """Cria uma tabela que contém as informações de criação do produto e sua modificação"""
    created = models.DateTimeField('criado em', auto_now=False, auto_now_add=True)
    modified = models.DateTimeField('modificado em', auto_now_add=False, auto_now=True)

    class Meta:
        abstract = True
