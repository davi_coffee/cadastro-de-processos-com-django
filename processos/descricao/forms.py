from django import forms
from .models import Description

# Criação de uma formulário
class DescriptionForm(forms.ModelForm):

    class Meta:
        model = Description
        # Pega todos os campos da tabela Description.
        fields = '__all__'