from django.shortcuts import render, HttpResponse, redirect
from django.http import HttpResponseRedirect
from .models import Description
from django.urls import reverse
from django.contrib.auth.decorators import login_required
# Formulários
from django.views.generic import CreateView
from .forms import DescriptionForm


@login_required(login_url='core:logar_user')
# Função: exibe a página inical dos processo, ou seja, a lista.
def processo_list(request):
    # Template_name: layout da página que é escrito em .html
    template_name = 'processo_list.html'
    # Pega o objetos da tabela Description.
    objects = Description.objects.all()
    # Dicionário que possui como valor, uma lista dos campos da tabela Description
    context = {'object_list_dict': objects}

    # Retorna a requisição, pagina html e dicionario.
    return render(request, template_name, context)


# Função: exibe a pagina de detalhes do processo através do id (pk) fornecido.
def processo_detail(request, pk):
    template_name = 'processo_detail.html'
    # Pega os campos do id fornecido (pk) da tabela Description
    obj = Description.objects.get(pk=pk)

    context = {'object_detail': obj}

    return render(request, template_name, context)


# Função: apagar um objeto caso for requisitado.
def processo_delete(request, id):
    post = Description.objects.get(pk=id)
    post.delete()
    return redirect('description:processo_list')


# Página de exibição do formulário.
def produto_add(request):
    form = DescriptionForm(request.POST or None)
    template_name = 'processo_form.html'

    # Confirmar se o formulario está adequado
    if request.GET == 'POST':
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('description:processo_list'))

    context = {'form': form}
    return render(request, template_name, context)


# Cria um formulário que vai ser fornecido pela função produto_add
class ProcessoCreate(CreateView):
    model = Description
    template_name = 'processo_form.html'
    form_class = DescriptionForm





