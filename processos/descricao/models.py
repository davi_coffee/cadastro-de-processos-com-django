from django.db import models
from django.urls import reverse_lazy


class Description(models.Model):
    """Cria uma tabela com as descrições do processo."""
    pasta = models.PositiveIntegerField('Proc- ', unique=True)
    escritorio_responsavel = models.CharField('Escritório responsável', max_length=15, default='NPJ-UNIVAG')
    escritorio_origem = models.CharField('Escritório de Origem', max_length=15, default='NPJ-UNIVAG', null=True, blank=True)
    codigo_processo = models.CharField('Código do processo', max_length=10, default='PJE', null=True, blank=True)
    cliente = models.CharField('Cliente no NPJ-UNIVAG', max_length=100)
    contrario = models.CharField('Contrário', max_length=100)
    tipo_action = models.CharField('Tipo de ação', max_length=100)
    n_principal = models.PositiveIntegerField('Nº Principal', null=True, blank=True)
    orgao_principal = models.CharField('Orgão', max_length=128, null=True, blank=True)
    professor = models.CharField('Professor Orientador', max_length=100, default='BRUNO HENRIQUE DA ROCHA', null=True, blank=True)
    natureza = models.CharField('Natureza', max_length=100)
    status = models.BooleanField('Encerrado', default=False)


    class Meta:
        ordering = ('cliente',)

    def __str__(self):
        # Esta função permite que a saída seja legível para o humano, ou seja, o que vai ser requirido no Description.objects.all()
        return self.cliente

    # Para ir ao url quando chamar a função com o id.
    def get_absolute_url(self):
        return reverse_lazy('description:processo_detail', kwargs={'pk': self.pk})

    # Retorna uma saída com o dado Cliente Formatado de forma elegante.
    def cliente_formatado(self):
        nome = self.cliente
        return str(nome.title())

    # Retorna uma saída com o dado do n_principal de forma organizada.
    def n_principal_formatado(self):
        if len(str(self.n_principal)) >= 16:
            numero = str(self.n_principal)
            numero = numero.strip()
            parte_1 = numero[:7]
            parte_2 = numero[7:9]
            parte_3 = numero[9:13]
            parte_4 = numero[13]
            parte_5 = numero[14:16]
            parte_6 = numero[16:]
            numero_formatado = parte_1 + '-' + parte_2 + '.' + parte_3 + '.' + parte_4 + '.' + parte_5 + '.' + parte_6
        else:
            numero_formatado = False
        return numero_formatado

    # Retorna o dado pasta formatado de forma elegante.
    def pasta_formatado(self):
        # zfill(number_max_digitos), com zero preenchido a esquerda.
        return str(self.pasta).zfill(7)

