from django.urls import path
from . import views

# Nome do aplicativo
app_name = 'description'

# Urls, 'nome', views.definição_do_Que_vai_ser_visto, name='nome_para_chamar_url'
urlpatterns = [
    path('', views.processo_list, name='processo_list'),
    path('<int:pk>/', views.processo_detail, name='processo_detail'),
    path('add/', views.ProcessoCreate.as_view(), name='processo_add'),
    path('deletar/<int:id>/', views.processo_delete, name='processo_delete'),
]