from django.contrib import admin
from .models import Description

# Exibe na tela do administrador de forma customizada.
@admin.register(Description)
class Description_Admin(admin.ModelAdmin):
    list_display = (
        'pasta',
        'escritorio_responsavel',
        'escritorio_origem',
        'codigo_processo',
        'cliente',
        'contrario',
        'tipo_action',
        'n_principal',
        'orgao_principal',
        'professor',
        'natureza',
        'status',
    )
    # Cria um campo de busca pelo camplos 'cliente'
    search_fields = ('cliente',)
    # Cria um filtro
    list_filter = ('orgao_principal',)
